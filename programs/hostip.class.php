<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


/**
 * Result from HostIp webservice
 * @link http://www.hostip.info/
 */
class LibGeoname_HostIp {

	public $countryName;
	public $countryAbbrev;
	public $placeName;
	public $latitude;
	public $longitude;
}




class LibGeoname_HostIpWebServiceException extends Exception { }



/**
 * HostIp webservice
 * @link http://www.hostip.info/
 */
class LibGeoname_HostIpWebService {

	/**
	 * Webservice host
	 * @var string
	 */
	const HOST = 'api.hostip.info';

	/**
	 * Webservice timeout in seconds
	 * @var int			
	 */
	const ACCESS_TIMEOUT = 1;


	/**
	 * @param	string	$ip
	 * @return string | null
	 */
	private static function getXml($ip) {

		$fp = @fsockopen(self::HOST, 80);
		if (!$fp) {
			throw new LibGeoname_HostIpWebServiceException(
				sprintf(
					geon_translate('Error, the host %s is inaccessible, the webservice is down or access to external url has been disabled in php'), 
					self::HOST
				)
			);
			return null;
		} 

		$out = 	sprintf("GET /?ip=%s HTTP/1.1\r\n", urlencode($ip)); "GET / HTTP/1.1\r\n";
		$out .= sprintf("Host: %s\r\n", self::HOST);
		$out .= "Connection: Close\r\n\r\n";

		fwrite($fp, $out);
		stream_set_timeout($fp, 1);
		$res = fread($fp, 2000);

		$info = stream_get_meta_data($fp);
		fclose($fp);

		if ($info['timed_out']) {

			throw new LibGeoname_HostIpWebServiceException(
				sprintf(
					geon_translate('Error, the access to hostip webservice is too slow'), 
					self::HOST
				)
			);

			return null;
		} else {

			if (preg_match('/(<\?xml.*<\/HostipLookupResultSet>)/s',$res, $m)) {
				return $m[1];
			}

			throw new LibGeoname_HostIpWebServiceException(
				sprintf(
					geon_translate('Error, the hostip webservice returned an unknown xml structure'), 
					self::HOST
				)
			);

			return null;
		}
	}


	/**
	 * @param string	$ip
	 * @return LibGeoname_HostIp
	 */
	public static function getInfosFromIp($ip) {
		$xml = self::getXml($ip);

		if ($xml) {
		    
			$root = simplexml_load_string($xml);
			//$root->registerXPathNamespace('hostip', 'http://www.hostip.info/api');
			$root->registerXPathNamespace('gml', 'http://www.opengis.net/gml');

			$country = $root->xpath('//gml:featureMember//Hostip');
			$placename = $root->xpath('//gml:featureMember//Hostip/gml:name');
			$strcoord = $root->xpath('//gml:featureMember//Hostip/ipLocation/gml:pointProperty/gml:Point/gml:coordinates');
			
			

			if ($strcoord) {
				$coordinates = explode(',',$strcoord[0]);
			} else {
				$coordinates = array(null, null);
			}

			$infos = new LibGeoname_HostIp;

			$infos->countryName 	= (string) $country[0]->countryName;
			$infos->countryAbbrev 	= (string) $country[0]->countryAbbrev;
			$infos->placeName		= (string) $placename[0];
			$infos->latitude		= (float) $coordinates[1];
			$infos->longitude		= (float) $coordinates[0];
			
			return $infos;
		}

		return null;
	}
}