<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';


class geon_OrmException extends Exception {

}




class Func_GeoNames extends bab_functionality {



	public function getDescription() {
		return geon_translate('GeoNames data access');
	}

	/**
	 * test ORM dependence and include
	 * @return ORM_MySqlSet
	 */
	private function getOrmClass($classname) {
		$orm = @bab_functionality::get('LibOrm');
		if (false === $orm) {
			throw new geon_OrmException(geon_translate('This method require the LibOrm addon'));
		}

		$orm->initMysql();
		require_once dirname(__FILE__).'/orm.class.php';
		return bab_getInstance($classname);
	}


	/**
	 * Main geoname table
	 * @return geon_geonameSet

	public function getGeoNameOrmSet() {
		return $this->getOrmClass('geon_geonameSet');
	}
	 */


	/**
	 * Postal codes table
	 * @return geon_postalcodeSet
	 */
	public function getPostalCodeOrmSet() {
		return $this->getOrmClass('geon_postalcodeSet');
	}

	/**
	 * Timezones table
	 * @return geon_timezoneSet
	 */
	public function getTimeZoneOrmSet() {
		return $this->getOrmClass('geon_timezoneSet');
	}

	/**
	 * Countries table
	 * @return geon_countrySet
	 */
	public function getCountryOrmSet() {
		return $this->getOrmClass('geon_countrySet');
	}



	/**
	 * @return string
	 */
	private function getCountryNameCol() {
		$colname = 'country_en';

		if (isset($GLOBALS['babLanguage']) && 'fr' === $GLOBALS['babLanguage']) {
			$colname = 'country_fr';
		}

		return $colname;
	}



	/**
	 * Get the coutry name according to user language from the ISO code with a case insensitive test
	 * @param	string	$code	ISO 2 or 3 letter
	 * @return	string | false	english or french string or	return false if no result found
	 */
	public function getCountryNameFromIso($code) {

		global $babDB;
		$isocol = 'iso';
		if (3 === strlen($code)) {
			$isocol = 'iso3';
		}

		$colname = $this->getCountryNameCol();

		$res = $babDB->db_query('SELECT '.$babDB->backTick($colname).' name FROM geon_country WHERE '.$babDB->backTick($isocol).' LIKE \''.$babDB->db_escape_like($code).'\'');

		if (1 !== $babDB->db_num_rows($res)) {
			return false;
		}

		$arr = $babDB->db_fetch_assoc($res);
		return $arr['name'];
	}


	/**
	 * Get the country row entry from the country name, case insensitive
	 *
	 * @param	string	$name		english or french string according to user options (if not french, english is used)
	 * @return	geon_CountryRecord | false		return false if no result found
	 */
	public function getCountryFromName($name) {
		global $babDB;
		require_once dirname(__FILE__).'/iterator.class.php';

		$colname = $this->getCountryNameCol();
		$query = 'SELECT * FROM geon_country WHERE '.$babDB->backTick($colname).' LIKE \''.$babDB->db_escape_like($name).'\'';
		$res = $babDB->db_query($query);

		if (1 !== $babDB->db_num_rows($res)) {
			return false;
		}

		$arr = $babDB->db_fetch_assoc($res);
		return new geon_CountryRecord($arr);
	}


	/**
	 * Get the country row entry from the country name, case insensitive
	 *
	 * @param	string	$name		portion of the country name, english or french string according to user options (if not french, english is used)
	 * @return	geon_CountryIterator
	 */
	public function searchCountryFromName($name) {
		require_once dirname(__FILE__).'/iterator.class.php';

		global $babDB;
		$colname = $this->getCountryNameCol();

		$query = '
			SELECT
				*,
				'.$babDB->backTick($colname).' LIKE \''.$babDB->db_escape_like($name).'%\' AS relevance
			FROM geon_country
			WHERE '.$babDB->backTick($colname).' LIKE \'%'.$babDB->db_escape_like($name).'%\'

			ORDER BY relevance DESC, '.$babDB->backTick($colname).' ASC
		';

		$res = $babDB->db_query($query);

		$iterator = new geon_CountryIterator;
		$iterator->setMySqlResult($res);

		return $iterator;
	}


	/**
	 * Search places with the begining of a postal code
	 * @param	string	$code		postal code or portion of postal code
	 * @param	string	$country	2 letter country code
	 * @return geon_PostalCodeIterator
	 */
	public function searchPlaceFromPostalCode($code, $country = null) {
		global $babDB;

		$colname = $this->getCountryNameCol();

		$query = 'SELECT
				p.*,
				c.'.$colname.' country

			FROM
				geon_postalcode p
					LEFT JOIN geon_country c ON c.iso = p.countrycode

			WHERE postalcode LIKE \''.$babDB->db_escape_like($code).'%\'';

		if (null !== $country) {
			$query .= ' AND countrycode LIKE \''.$babDB->db_escape_like($country).'\'';
		}

		$query .= ' ORDER BY postalcode';


		return $this->searchPlace($query);
	}

	/**
	 * Search places with a portion of the name
	 * @param	string	$name
	 * @param	string	$country	2 letter country code
	 * @return geon_PostalCodeIterator
	 */
	public function searchPlaceFromName($name, $country = null) {
		global $babDB;

		$colname = $this->getCountryNameCol();

		$query = 'SELECT
			p.*,
			p.placename LIKE \''.$babDB->db_escape_like($name).'%\' AS relevance,
			c.'.$colname.' country
		FROM
			geon_postalcode p
				LEFT JOIN geon_similarities s ON s.placename1 = p.placename
				LEFT JOIN geon_country c ON c.iso = p.countrycode
		WHERE
			(p.placename LIKE \'%'.$babDB->db_escape_like($name).'%\'
			OR s.placename2 LIKE \''.$babDB->db_escape_like($name).'%\')';


		if (null !== $country) {
			$query .= ' AND p.countrycode LIKE \''.$babDB->db_escape_like($country).'\'';
		}

		$query .= ' GROUP BY p.placename, p.postalcode ORDER BY relevance DESC, placename ASC';

		return $this->searchPlace($query);
	}

	/**
	 * @param	string	$query_part
	 * @param	string	$country	2 letter country code
	 * @return geon_PostalCodeIterator
	 */
	private function searchPlace($query) {
		require_once dirname(__FILE__).'/iterator.class.php';

		global $babDB;

		$res = $babDB->db_query($query);

		$iterator = new geon_PostalCodeIterator;
		$iterator->setMySqlResult($res);

		return $iterator;
	}



	/**
	 * Find the most nearby place from latitude longiture
	 * the returned object geon_PostalCodeRecord contain an additional key "distance" with the distance
	 * in kilomter beetween the search point and the nearby place
	 *
	 * @param	string	$latitude
	 * @param	string	$longitude
	 * @return	geon_PostalCodeRecord
	 */
	public function getNearPlace($latitude, $longitude) {

		require_once dirname(__FILE__).'/iterator.class.php';

		global $babDB;

		$query = '
			SELECT
				*,
				SQRT(
					POW(latitude - '.$babDB->quote($latitude).', 2) +
					POW(longitude - '.$babDB->quote($longitude).', 2)
				) distance
			FROM
				geon_postalcode
			ORDER BY distance ASC LIMIT 0,1
		';

		$res = $babDB->db_query($query);
		$arr = $babDB->db_fetch_assoc($res);

		// convert distance in nautic miles to km
		$arr['distance'] = 111.111 * (float) $arr['distance'];

		return new geon_PostalCodeRecord($arr);
	}





    /**
     * Get localisations informations from an IP address using the HostIp webservice
     * the returned object will have the properties :
     * <ul>
     * <li>countryName : english country name</li>
     * <li>countryAbbrev : 2 letter country code</li>
     * <li>placeName : city name if available</li>
     * <li>latitude : latitude if available as float</li>
     * <li>longitude : longitude if available as float</li>
     * </ul>
     *
     * @link http://www.hostip.info/
     *
     * @param string $ip
     * @return LibGeoname_HostIp @throw LibGeoname_HostIpWebServiceException
     */
    public function hostIpWebService($ip)
    {
        require_once dirname(__FILE__) . '/hostip.class.php';
        return LibGeoname_HostIpWebService::getInfosFromIp($ip);
    }


    /**
     * Get country from IP using the db-ip database
     *
     * @link https://db-ip.com/
     *
     * @param string $ip
     *
     * @return geon_CountryRecord
     */
    public function ipLookup($ip)
    {
        require_once dirname(__FILE__) . '/iterator.class.php';
        global $babDB;

        $query = 'select c.* from
	        `geon_lookup` l
	           LEFT JOIN `geon_country` c ON c.iso = l.country
	    where
	        l.addr_type = ' . $babDB->quote(geon_addrType($ip)) . '
	        AND l.ip_start <= ' . $babDB->quote(inet_pton($ip)) . '
	    order by l.ip_start desc limit 1';

        $res = $babDB->db_query($query);

        if (1 !== $babDB->db_num_rows($res)) {
            return false;
        }

        $arr = $babDB->db_fetch_assoc($res);
        return new geon_CountryRecord($arr);
    }


    /**
     *
     * @param string    $location
     * @param bool      $debug
     * @return string[]
     */
    public function getGoogleGeocode($location, $debug = false)
    {
        $geocoder = "http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false";

        $location = bab_convertStringFromDatabase($location, 'UTF-8');

        // Penser a encoder votre adresse
        $location = urlencode($location);

        $query = sprintf($geocoder, $location);

        // On interroge le serveur
        $results = file_get_contents($query);

        $arrResults = json_decode($results, true);
        if ($debug) {
            echo $query;
            var_dump($arrResults);
        }

        $longitude = $arrResults['results'][0]['geometry']['location']['lng'];
        $latitude = $arrResults['results'][0]['geometry']['location']['lat'];

        $returnResults = array(
            'longitude' => $longitude,
            'latitude' => $latitude
        );

        return $returnResults;
    }


    public function getGeocode($location, $service = 'google', $debug = false)
    {
        if (! $location) {
            return array();
        }

        if ($service == 'google') {
            return $this->getGoogleGeocode($location, $debug);
        }

        return array();
    }


    /**
     *
     * @param string    $city
     * @param string    $postalcod
     * @param string    $street
     * @param string    $country
     * @param bool      $debug
     * @param string    $service
     * @return string[]
     */
    public function getAddressCoordinates($city = null, $postalCode = null, $street = null, $country = null, $debug = false, $service = null)
    {
        if (!isset($service)) {
            $service = bab_Registry::get('/libgeonames/geocoder', 'ban');
        }
        switch ($service) {
            case 'google':
                $geocoder = new geon_GeoCoderGoogle();
                break;

            case 'nominatim':
                $geocoder = new geon_GeoCoderNominatim();
                break;

            case 'ban':
                $geocoder = new geon_GeoCoderBan();
                break;

            default:
                return array();
        }

        $coordinates = $geocoder->getAddressCoordinates($city, $postalCode, $street, $country);

        return $coordinates;
    }
}






abstract class geon_GeoCoder
{
    /**
     *
     * @param string    $city
     * @param string    $postalCode
     * @param string    $street
     * @param string    $country
     * @return string[]
     */
    public abstract function getAddressCoordinates($city = null, $postalCode = null, $street = null, $country = null);
}



class geon_GeoCoderBan extends geon_GeoCoder
{
    /**
     *
     * @param string $string
     * @return string
     */
    private static function str($string)
    {
        return urlencode(bab_convertStringFromDatabase($string, 'UTF-8'));
    }


    /**
     *
     * @param string    $city
     * @param string    $postalCode
     * @param string    $street
     * @param string    $country
     * @return string[]
     */
    public function getAddressCoordinates($city = null, $postalCode = null, $street = null, $country = null)
    {
        if(empty($street)){
            return array(
                'longitude' => null,
                'latitude' => null
            );
            
        }
        $geocoder = "https://api-adresse.data.gouv.fr/search/?q=%s&postcode=%s";

        $query = sprintf($geocoder, self::str($street), self::str($postalCode));

        $opts = array('http' => array('header' => "User-Agent: Ovidentia\r\n"));
        $context = stream_context_create($opts);

        $results = file_get_contents($query, false, $context);

        $results = json_decode($results, true);

        $longitude = $results['features'][0]['geometry']['coordinates'][0];
        $latitude = $results['features'][0]['geometry']['coordinates'][1];

        $returnResults = array(
            'longitude' => $longitude,
            'latitude' => $latitude
        );

        return $returnResults;
    }
}


class geon_GeoCoderNominatim extends geon_GeoCoder
{
    /**
     *
     * @param string $string
     * @return string
     */
    private static function str($string)
    {
        return urlencode(bab_convertStringFromDatabase($string, 'UTF-8'));
    }


    /**
     *
     * @param string    $city
     * @param string    $postalCode
     * @param string    $street
     * @param string    $country
     * @return string[]
     */
    public function getAddressCoordinates($city = null, $postalCode = null, $street = null, $country = null)
    {
        
        $geocoder = "https://nominatim.openstreetmap.org/search?format=json&street=%s&city=%s&postalcode=%s&country=%s";

        $query = sprintf($geocoder, self::str($street), self::str($city), self::str($postalCode), self::str($country));

        $opts = array('http' => array('header' => "User-Agent: Ovidentia\r\n"));
        $context = stream_context_create($opts);

        $results = file_get_contents($query, false, $context);

        $results = json_decode($results, true);

//        bab_debug($results);

        $longitude = $results[0]['lon'];
        $latitude = $results[0]['lat'];

        $returnResults = array(
            'longitude' => $longitude,
            'latitude' => $latitude
        );

        return $returnResults;
    }
}


class geon_GeoCoderGoogle extends geon_GeoCoder
{


    public function getGeocode($location)
    {
        $geocoder = "http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false";

        $location = bab_convertStringFromDatabase($location, 'UTF-8');

        // Penser a encoder votre adresse
        $location = urlencode($location);

        $query = sprintf($geocoder, $location);

        // On interroge le serveur
        $results = file_get_contents($query);

        $arrResults = json_decode($results, true);

//         if ($debug) {
//             echo $query;
//             var_dump($arrResults);
//         }

        $longitude = $arrResults['results'][0]['geometry']['location']['lng'];
        $latitude = $arrResults['results'][0]['geometry']['location']['lat'];

        $returnResults = array(
            'longitude' => $longitude,
            'latitude' => $latitude
        );

        return $returnResults;
    }

    /**
     *
     * @param string    $city
     * @param string    $postalCode
     * @param string    $street
     * @param string    $country
     * @return string[]
     */
    public function getAddressCoordinates($city = null, $postalCode = null, $street = null, $country = null)
    {
        $location = '';
        if ($street) {
            $location = $street . ' ';
        }
        if ($postalCode) {
            $location .= $postalCode. ' ';
        }
        if ($city) {
            $location .= $city . ' ';
        }
        if ($country) {
            $location .= $country;
        }

        $coordinates = $this->getGeocode($location);

        return $coordinates;
    }
}

