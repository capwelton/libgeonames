; <?php/*
[general]
name                        ="LibGeoNames"
version                     ="0.1.3"
encoding                    ="UTF-8"
description                 ="shared data : countries, cities ..."
description.fr              ="Librairie partagée fournissant des données de géolocalisation"
long_description.fr         ="README.md"
mysql_character_set_database="latin1,utf8"
delete                      =1
longdesc                    =""
ov_version                  ="7.1.91"
php_version                 ="5.1.0"
mysql_version               ="4.1.2"
addon_access_control        =0
preinstall_script           ="preinstall.php"
author                      ="Paul de Rosanbo / http://www.geonames.org/"
icon                        ="galeon.png"
configuration_page          ="configuration"
tags                        ="library,geo,data"
;*/?>
