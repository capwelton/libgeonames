<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';





$G = bab_functionality::get('GeoNames');



$postalcodeset = $G->getPostalCodeOrmSet();
bab_debug($postalcodeset->get($postalcodeset->postalcode->is('78100'))->placename);


// villes avec code postal commencant par 782
/*
$trace = '';
foreach( $postalcodeset->select($postalcodeset->postalcode->startWith('782'))->orderAsc($postalcodeset->placename) as $record) {
	$trace .= $record->postalcode." \t ".$record->placename."\n";
}

bab_debug($trace);
*/

// pays europ�en ordonn�s par population
/*
$countryset = $G->getCountryOrmSet(); 
$countries = $countryset->select($countryset->continent->is('EU'))->orderDesc($countryset->population);

$trace = '';
foreach($countries as $country) {
	$trace .= $country->iso." \t ".$country->country."\n";
}

bab_debug($trace);

*/



$trace = '';
$I = $G->searchPlaceFromPostalCode('8512'); 

foreach($I as $record) {
	$trace .= $record->postalcode." \t ".$record->placename." \t ".$record->latitude." \t ".$record->longitude."\n";
}

bab_debug($trace);

// rechercher une ville par latitude longitude

bab_debug($G->getNearPlace('46.5365000', '-0.7843000'));


//trouver dans informations en fonction de l'ip
try {
	$infos = $G->hostIpWebService('213.215.40.194');
	bab_debug($infos);

	// from coordinates :
	bab_debug($G->getNearPlace($infos->latitude, $infos->longitude));
	
} catch (Exception $e) {
	bab_debug($e->getMessage());
}
